# RD LAB FE14 NODEJS HOMEWORK №2

## Use NodeJS to implement web server.

### Requirements:
- Use express to implement web-server;
- Use express Router for scaling your app and `MVC` pattern to organize project structure;
- Use jsonwebtoken package for jwt authorization;
- Ability to run server on port which is defined as `PORT` env variable;
- Mandatory npm start script.

### Acceptance criteria:
- Ability to register users;
- User is able to login into the system;
- User is able to view only personal notes;
- User is able to add, delete personal notes;
- User can check/uncheck any note;
- User can manage notes and personal profile only with valid JWT token in request;
- `Gitlab` repo link and project id are saved in Google spreadsheets by [link](https://docs.google.com/spreadsheets/d/1n91rWSw76qjC2KIHXd5CpdegQKaBbolVo_vbByCB5hk/edit#gid=0).

### Optional criteria:
- User is able to view his profile info;
- User can change his profile password;
- User can delete personal account;
- Ability to edit personal notes text;
- Simple UI for your application(would be a big plus).
