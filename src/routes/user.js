const express = require('express');

const auth = require('../middlewares/auth');
const controllers = require('../controllers/user');

const router = express.Router();

router.get('/me', auth, controllers.getById);
router.delete('/me', auth, controllers.remove);
router.patch('/me', auth, controllers.update);

module.exports = router;
