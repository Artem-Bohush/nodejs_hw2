const express = require('express');

const auth = require('../middlewares/auth');
const controllers = require('../controllers/note');

const router = express.Router();

router.get('/', auth, controllers.getAll);
router.get('/:id', auth, controllers.getById);
router.post('/', auth, controllers.create);
router.put('/:id', auth, controllers.update);
router.patch('/:id', auth, controllers.checkUncheck);
router.delete('/:id', auth, controllers.remove);

module.exports = router;
