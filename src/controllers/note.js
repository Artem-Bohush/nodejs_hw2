const Note = require('../models/Note');
const handleError = require('../utils/handleError');

module.exports.getAll = async (req, res) => {
  try {
    let notes = await Note.find({ userId: req.user.userId });
    notes = notes.map((n) => ({
      _id: n._id,
      userId: n.userId,
      completed: n.completed,
      text: n.text,
      createdDate: n.createdDate,
    }));
    return res.status(200).json({ notes });
  } catch (e) {
    return handleError(res, e);
  }
};

module.exports.getById = async (req, res) => {
  try {
    let note = await Note.findById(req.params.id);

    if (!note) {
      return res.status(400).json({ message: 'Wrong note id' });
    }

    note = {
      _id: note._id,
      userId: note.userId,
      completed: note.completed,
      text: note.text,
      createdDate: note.createdDate,
    };

    return res.status(200).json({ note });
  } catch (e) {
    return handleError(res, e);
  }
};

module.exports.create = async (req, res) => {
  try {
    const { text } = req.body;

    if (!text) {
      return res.status(400).json({ message: 'The "text" field is missing in the body' });
    }

    await new Note({
      userId: req.user.userId,
      text,
    }).save();

    return res.status(200).json({ message: 'Success' });
  } catch (e) {
    return handleError(res, e);
  }
};

module.exports.update = async (req, res) => {
  try {
    const { text } = req.body;

    if (!text) {
      return res.status(400).json({ message: 'The "text" field is missing in the body' });
    }

    const updated = await Note.findOneAndUpdate(
      { _id: req.params.id },
      { $set: { text } },
      { new: true },
    );

    if (!updated) {
      return res.status(400).json({ message: 'Wrong note id' });
    }

    return res.status(200).json({ message: 'Success' });
  } catch (e) {
    return handleError(res, e);
  }
};

module.exports.checkUncheck = async (req, res) => {
  try {
    const note = await Note.findById(req.params.id);

    if (!note) {
      return res.status(400).json({ message: 'Wrong note id' });
    }

    note.completed = !note.completed;

    await note.save();
    return res.status(200).json({ message: 'Success' });
  } catch (e) {
    return handleError(res, e);
  }
};

module.exports.remove = async (req, res) => {
  try {
    const { deletedCount } = await Note.deleteOne({ _id: req.params.id });

    if (deletedCount) {
      return res.status(200).json({ message: 'Success' });
    }

    return res.status(400).json({ message: 'Wrong note id' });
  } catch (e) {
    return handleError(res, e);
  }
};
