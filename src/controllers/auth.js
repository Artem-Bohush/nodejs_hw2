const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../models/User');
const Credentials = require('../models/Credentials');
const handleError = require('../utils/handleError');

module.exports.login = async (req, res) => {
  try {
    const { username, password } = req.body;

    if (!username || !password) {
      return res.status(400).json({ message: 'The "username" or "password" field is missing in the body' });
    }

    const [user, credentials] = await Promise.all([
      await User.findOne({ username: req.body.username }),
      await Credentials.findOne({ username: req.body.username }),
    ]);

    if (user) {
      const comparingResult = bcrypt.compareSync(req.body.password, credentials.password);

      if (!comparingResult) {
        return res.status(400).json({
          message: 'Passwords do not match',
        });
      }

      const token = jwt.sign(
        {
          username: user.username,
          userId: user._id,
          credentialsId: credentials._id
        },
        process.env.JWT_SECRET,
        { expiresIn: 60 * 60 },
      );

      return res.status(200).json({ jwt_token: token });
    }

    return res.status(400).json({
      message: `User with '${req.body.username}' username was not found`,
    });
  } catch (e) {
    return handleError(res, e);
  }
};

module.exports.register = async (req, res) => {
  try {
    const { username, password } = req.body;

    if (!username || !password) {
      return res.status(400).json({ message: 'The "username" or "password" field is missing in the body' });
    }

    const candidate = await User.findOne({ username: req.body.username });

    if (candidate) {
      return res.status(400).json({ message: `Username '${req.body.username}' is already taken` });
    }

    const salt = bcrypt.genSaltSync(10);

    const userPromise = new User({
      username: req.body.username,
    }).save();

    const credentialsPromise = new Credentials({
      username: req.body.username,
      password: bcrypt.hashSync(password, salt),
    }).save();

    await Promise.all([userPromise, credentialsPromise]);

    return res.status(200).json({ message: 'Success' });
  } catch (e) {
    return handleError(res, e);
  }
};
