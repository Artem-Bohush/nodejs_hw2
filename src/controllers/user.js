const bcrypt = require('bcryptjs');

const Note = require('../models/Note');
const User = require('../models/User');
const Credentials = require('../models/Credentials');
const handleError = require('../utils/handleError');

module.exports.getById = async (req, res) => {
  try {
    const user = await User.findById(req.user.userId);
    return res.status(200).json({
      user: {
        _id: user._id,
        username: user.username,
        createdDate: user.createdDate,
      },
    });
  } catch (e) {
    return handleError(res, e);
  }
};

module.exports.update = async (req, res) => {
  try {
    const { oldPassword, newPassword } = req.body;

    if (!oldPassword || !newPassword) {
      return res.status(400).json({ message: 'The "oldPassword" or "newPassword" field is missing in the body' });
    }

    const candidate = await User.findById(req.user.userId);

    if (!candidate) {
      return res.status(400).json({ message: 'Wrong user id' });
    }

    const credentials = await Credentials.findOne({ username: candidate.username });

    const comparingResult = bcrypt.compareSync(oldPassword, credentials.password);

    if (comparingResult) {
      await Credentials.findOneAndUpdate(
        { username: credentials.username },
        { $set: { password: bcrypt.hashSync(newPassword, bcrypt.genSaltSync(10)) } },
      );
      return res.status(200).json({ message: 'Success' });
    }

    return res.status(400).json({ message: 'Wrong old password' });
  } catch (e) {
    return handleError(res, e);
  }
};

module.exports.remove = async (req, res) => {
  try {
    const { deletedCount } = await User.deleteOne({ _id: req.user.userId });

    if (deletedCount) {
      await Promise.all([
        Credentials.deleteOne({ username: req.user.username }),
        Note.deleteMany({ userId: req.user.userId }),
      ]);
      return res.status(200).json({ message: 'Success' });
    }

    return res.status(400).json({ message: 'User does not exist' });
  } catch (e) {
    return handleError(res, e);
  }
};
