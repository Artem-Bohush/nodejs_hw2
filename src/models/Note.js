const mongoose = require('mongoose');

const { Schema } = mongoose;

const noteSchema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model('notes', noteSchema);
