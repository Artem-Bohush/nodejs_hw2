const mongoose = require('mongoose');

const { Schema } = mongoose;

const errorSchema = new Schema({
  message: {
    type: String,
    required: true,
    unique: true,
  },
});

module.exports = mongoose.model('errors', errorSchema);
