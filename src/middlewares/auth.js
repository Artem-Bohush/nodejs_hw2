const jwt = require('jsonwebtoken');

const handleError = require('../utils/handleError');

module.exports = async (req, res, next) => {
  try {
    const authHeader = req.headers.authorization;

    if (!authHeader) {
      return res.status(400).json({ message: '"authorization" header not found' });
    }

    const [, token] = authHeader.split(' ');

    try {
      const { username, userId, credentialsId } = jwt.verify(token, process.env.JWT_SECRET);
      req.user = { username, userId, credentialsId };
      next();
    } catch (e) {
      return res.status(400).json({ message: 'Invalid JWT' });
    }
  } catch (e) {
    return handleError(res, e);
  }
};
